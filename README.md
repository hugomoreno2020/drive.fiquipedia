Proyecto asociado a [https://gitlab.com/fiquipedia/fiquipedia](https://gitlab.com/fiquipedia/fiquipedia)

En la migración de FiQuiPedia en 2021 desde Google Sites a GitLab se almacenan en este proyecto GitLab ficheros que antes estaban en Google Drive

Eran ficheros voluminosos / numerosos / estáticos y hace que el despligue con CI/CD dentro de proyecto GitLab pages no sea viable:  
* Despliegue supone mucho volumen (hay un límite de 1 GB en modo no premium)
    `ERROR: Uploading artifacts as "archive" to coordinator... too large archive`
* consume mucho tiempo (hay un límite de CI/CD minuntes en modo no premium)
   * En julio 2021 durante migración indicaba 100 CI/CD minutes per month, luego veo 400, y este comentario 
   *Namespaces created before 2021-07-17 do not consume CI pipeline minutes.*   
Referencia documentación: 
- https://about.gitlab.com/pricing/
- https://docs.gitlab.com/ee/subscriptions/gitlab_com/index.html#ci-pipeline-minutes

Por lo tanto es un proyecto sin CI/CD: los ficheros se pueden acceder vía navegador y se pueden enlazar. Por ejemplo este mismo fichero

[https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/README.md](https://gitlab.com/fiquipedia/drive.fiquipedia/-/raw/main/README.md)

Con la migración se decide mover y almacenar aquí, en la carpeta "content", los ficheros pdf que estaban inicialmente en "content" https://gitlab.com/fiquipedia/fiquipedia, para minimizar tiempo de compilación, ya que en migración inicial ocupa unos 180 MB, y tiene mucho sentido que se actualicen solo aquí sin generar una compilación del sitio (puedo subir desde el navegador ficheros pdf, pero vía navegador es de uno en uno, y eso generaría una compilación por cada fichero, lo que no tiene sentido aparte de consumo de CI/CD minutes).  
Se migra con la misma estructura de directorios.  
Se mueven **todos** los ficheros pdf; eso implica que algunos enlaces y códigos QR dejan de ser válidos.
Para los ficheros pdf para los que hay código QR o su enlace se sabe que se usa en ciertos sitios, se usan redirecciones (ver comentarios migración en [https://gitlab.com/fiquipedia/fiquipedia](https://gitlab.com/fiquipedia/fiquipedia)

Como manejo muchos ficheros, en general intento que su nombre comience por la fecha y usar [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601), aunque a veces sea solo iniciando el nombre por el año.  

# Algo queda que decir
Dentro de la carpeta de Google Drive "fiquipedia" tenía una carpeta "algoquedaquedecir" asociada al blog [Algo queda que decir](http://algoquedaquedecir.blogspot.com/). Inicialmente me había planteado separar en varios repostorios, pero al final hay cosas que tienen relación (como por ejemplo temas de legislación educativa), así que lo unificaré de la misma manera con la carpeta https://gitlab.com/fiquipedia/drive.fiquipedia/-/tree/main/algoquedaquedecir, aunque es algo a largo plazo, porque el volumen es inmenso, y lo haré según vaya migrando el blog a markdown (creo un proyecto separado https://gitlab.com/fiquipedia/algoquedaquedecir) o según vaya escribiendo nuevos posts. En septiembre 2021 creo una primera subcarpeta al escribir un primer post en el blog que enlaza ya los ficheros aquí sin usar Google Drive. [Legitimación, civismo y transparencia](https://algoquedaquedecir.blogspot.com/2021/09/legitimacion-civismo-y-transparencia.html)  

# Temas pendientes:
* Búsqueda desde la página principal debe buscar también pueda buscar en los ficheros pdf que no están escaneados, algunos de los que antes estaban incluidos en la búsqueda de Google Sites (dentro de content) y otros que estaban fuera y antes en Google Sites no estaban incluidos en la búsqueda. En GCSE (Google Custom Search Console) que se usa desde tema hugo beautiful sí se puede añadir otro dominio de búsqueda, pero como ese buscador de Google añade publicidad, pendiente mirar otra solución
* Ver si crear subdominio drive.fiquipedia.es para que apunte a https://gitlab.com/fiquipedia/drive.fiquipedia/ 
* Al superar 1 GB, mirar https://docs.gitlab.com/ee/topics/git/lfs/ que indica  
*The general recommendation is to not have Git repositories larger than 1GB to preserve performance.*  
https://docs.gitlab.com/ee/topics/git/lfs/migrate_to_git_lfs.html




